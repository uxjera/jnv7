NOTES:

Landing:

	About Me

		> Location
		> Psychology + Technology

Design:

THEME: What is the problem to solve and how to approach it
	
	(What is Wanted:
		> Communication w/ Team
		> Wireframes and Mockups
		> Information Architecture
	)
	
	Websites and Design Insights

	|| Objective: Demonstrate programming proficiency and application of UX practices, 
	|| discuss processes used and reflect on the project and what could be improved 
	|| (or the next step in the project)

		> HeroicKnightGames
			.. Tech Used: Basic HTML/CSS/JS
			.. Project Timeline: Dec 2017 -> Feb 2018
		> SupportCarryZilean
			.. Tech Used: AngularJS (4) / SASS 
			.. Project Timeline: Aug 2017 -> Nov 2017 (maintained for 2 years)
		> JNv6 into 7
			.. ReactJS

	Content Strategy

	|| Objective: Demonstrate understanding of IA and discuss strategies based on your message

		> Information Architecture
		> Writing
		> KSN article as example (not sure which one)

	My Design Process

	|| Objective: Share insight of how I approach a project and how I work through it

		> Understanding the Problem and developing a solution
			.. When working with a small business, I want to get to know the business
					better, what are their needs? what are the customers needs? where do
					the two intersect? where are the disconnects?
		> Wireframes and how I think out a design

Research:

THEME: Problem to Solve => Metric to Measure Progress

	(What is Wanted:
		> Usability
		> Working within a Team
		> Excel
		> User Research (Target Demo, Personas)
	)

	Usability & User Identification

	|| Objective: Demonstrate experience thinking out and running a usability experiment. 
	|| Be careful on examples to not get in trouble with NDAs

		> Researching the user and defining target audience
			"When I ask small business who their target market is, young entrepenuers replay 'EVERYBODY!'. This is a crucial misunderstanding"
		> Running through the process of a Usability Study (both official and lean)
		> Interpreting the results

	Expermientation - VGATT

	|| Objective: Show direct Lab experience detailing my contribution to this experiment: 
	|| recruitment, design decision commentary, testing, data analysis

		> Re-write content to be similar to old article, except more depth in the article

	Problem Solving - LoL Comm systems

	|| Objective: Demonstrate versatility through experience as Analyst by taking 
	|| a deeper dive into the League of Legends pro-player communication system

		> 

