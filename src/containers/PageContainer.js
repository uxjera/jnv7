import React, {Component} from 'react';
import { withRouter } from 'react-router'
import { Route, Switch } from 'react-router-dom';

import Landing from '../components/Landing/Landing';
import Design from '../components/Design/Design';
import Research from '../components/Research/Research';
import About from '../components/About/About';

import './PageContainer.css';

class PageContainer extends Component {

	render() {

		let parsedURL = this.props.history.location.pathname.split('/')
		let backgroundClass = 'landing-bg';

		if (parsedURL.some((word) => word === 'research')){
				backgroundClass = 'research-bg'
			} else if (parsedURL.some((word) => word === 'design')) {
				backgroundClass = 'design-bg'
			} else if (parsedURL.some((word) => word === 'about')) {
				backgroundClass = 'about-bg'
			}

		return(
			<div 
			className={backgroundClass}
			style={{
				minHeight: '80vh',
				display: 'flex',
				flexDirection: 'column',
				justifyContent: 'flex-start',
				alignItems: 'center'
			}}>

        <Switch>

        	<Route exact path="/" component={Landing} />
        	<Route path="/design" component={Design}  />
        	<Route path="/research" component={Research} />
        	<Route path="/about" component={About} />
        	
        </Switch>
			</div>
		)
	}
}

export default withRouter(PageContainer);