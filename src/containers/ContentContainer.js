import React, {Component} from 'react';
import {Route, Switch } from 'react-router-dom';

import Landing from '../components/Landing/Landing';
import Design from '../components/Design/Design';
import Research from '../components/Research/Research';
import About from '../components/About/About';

import bg from '../assets/images/background.png'

export default class PageContainer extends Component {
	constructor(props){
		super(props);
		this.state=({
			background: 'a string',
		})
	}

	componentDidMount(){
		console.log(this.props.location)
	}

	render() {
		
		return(
			<div style={{
				backgroundImage: `url(${bg})`,
				minHeight: '80vh',
				display: 'flex',
				justifyContent: 'center',
			}}>
        <Switch>
        	<Route exact path="/" component={Landing} />
        	<Route path="/design" component={Design}  />
        	<Route path="/research" component={Research} />
        	<Route path="/about" component={About} />
        	
        </Switch>
			</div>
		)
	}
}