export const GET_LOCATION = "GET_LOCATION";

function handleGetLocation(location){
	return{
		type: GET_LOCATION,
		location
	}
}

export function getLocation(){
	return dispatch => {
		location: this.props.location.pathname
	}
}