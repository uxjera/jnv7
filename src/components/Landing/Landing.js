import React from 'react';
import LandingContent from './LandingContent/LandingContent';
import './Landing.css';

const Landing = () => (
	<div className="primary-container">
		<LandingContent />
	</div>
)

export default Landing;
