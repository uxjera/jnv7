import React from 'react';
import './LandingContent.css';
import Buttons from '../../Public/Buttons/Buttons';

function alertClick (e) {
	e.preventDefault();
	console.log("This button was clicked")
}

const LandingContent = () => (
	<div className="landing-container">
		<div className="landing-content">
			<h1>WELCOME TO MY WEBSITE</h1>
			<p>This is LandingContent</p>
			<Buttons name="this is a button" fn={alertClick} type="success" />
		</div>
	</div>
)

export default LandingContent;
