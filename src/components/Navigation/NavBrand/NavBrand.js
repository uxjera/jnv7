import React from 'react';
import './NavBrand.css';
import {NavLink} from 'react-router-dom';

const NavBrand = () => (
	<div className="brand">
		<NavLink to="/">JN</NavLink>
	</div>
)

export default NavBrand;