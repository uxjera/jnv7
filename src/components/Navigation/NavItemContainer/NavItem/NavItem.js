import React from 'react';
import {NavLink} from 'react-router-dom';
import './NavItem.css';

const NavItem = ({name, link}) => (
	<li>
		<NavLink to={link}>{name}</NavLink>
	</li>
)

export default NavItem;