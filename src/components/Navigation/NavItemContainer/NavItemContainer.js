import React, {Component} from 'react';
import NavItem from './NavItem/NavItem';
import './NavItemContainer.css';

class NavItemContainer extends Component {
	static defaultProps = {
		navItems: [
		{name: 'Design', link: '/design'},
		{name: 'Research', link: '/research'}, 
		{name: 'About', link: '/about'}
		]
	}
	render(){
		return(
		<div className="nav-item-container">
			{this.props.navItems.map((item, index) => (
				<NavItem 
				key={index} 
				name={item.name} 
				link={item.link} />
			))}
		</div>
	)}
}

export default NavItemContainer;