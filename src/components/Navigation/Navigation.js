import React, {Component} from 'react';
import NavBrand from './NavBrand/NavBrand';
import NavItemContainer from './NavItemContainer/NavItemContainer';
import './Navigation.css';

class Navigation extends Component {
	render() {
		return(
			<nav className="navigation-container">
				<ul> 
					<NavBrand />
					<NavItemContainer />
				</ul>
			</nav>
		)
	}
}

export default Navigation;