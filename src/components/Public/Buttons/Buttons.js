import React from 'react';
import './Buttons.css';

const Button = ({name, fn, type}) => (
	<button type="button" onClick={fn} className={type}>
		{name}
	</button>
)

export default Button;