import React, {Component} from 'react';
import {Route } from 'react-router-dom';
import './Research.css';
import ResearchNav from './ResearchNav';
import Research1 from './ResearchViews/Research1';
import Research2 from './ResearchViews/Research2';
import Research3 from './ResearchViews/Research3';
import ResearchDefault from './ResearchViews/ResearchDefault'; 

class Research extends Component {
	render(){
		let currentURL = this.props.location.pathname;
		return(
			<div className="page-container">
				<div className="primary-container">
					<ResearchNav currentURL = {currentURL} 
					/>
						<div className="content-container">
					<Route exact path={this.props.match.path} component={ResearchDefault} />
      		<Route path={`${this.props.match.path}/research1`} component={Research1} />
      		<Route path={`${this.props.match.path}/research2`} component={Research2} />
      		<Route path={`${this.props.match.path}/research3`} component={Research3} />
      			</div>
				</div>
			</div>
		)
	}
}

export default Research;
