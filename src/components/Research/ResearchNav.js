import React, {Component} from 'react';
import { Link } from 'react-router-dom';

const items = [
	{
		name: 'Research 1',
		id: 'research1',
		img: '',
		description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Necessitatibus, cupiditate!',
		link: 'research1',
	},
	{
		name: 'Research 2',
		id: 'research2',
		img: '',
		description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus perspiciatis mollitia, perferendis expedita molestias, minus.',
		link: 'research2',
	},
	{
		name: 'Research 3',
		id: 'research3',
		img: '',
		description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
		link: 'research3',
	}
];

export default class DesignNav extends Component {

	render(){
		const {currentURL} = this.props;
		return(
			<div style={{
				background: 'red',
				}}>
					<ul
					className='subnav-container'
					style={{display: 'flex'}}>
					{items.map((d) => (
						<li 
						key={d.id} 
						style={{
							flex: '1',
							display: 'flex',
							alignItems: 'center',
							flexDirection: 'column',
						}}
						className={('/research/'+d.link === currentURL ? 'active' : '')}>
						<h3 style={{fontFamily: "'Righteous', cursive"}}>
							<Link to={`/research/${d.link}`}
							style={{
								textTransform: 'capitalize',
								textAlign: 'center'
							}}
							>{d.name}</Link></h3>
							{currentURL !== '/research' ?
								<p>CurrentURL = {currentURL}</p> : 
								<p>{d.description}</p>
							}
						</li>
					))}
				</ul>
			</div>
		)
	}
}