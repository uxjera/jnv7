import React, {Component} from 'react';
import { Link } from 'react-router-dom';

const items = [
	{
		name: 'Finished Projects',
		id: 'design1',
		img: '',
		description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Necessitatibus, cupiditate!',
		link: 'design1',
	},
	{
		name: 'Content Strategy',
		id: 'design2',
		img: '',
		description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus perspiciatis mollitia, perferendis expedita molestias, minus.',
		link: 'design2',
	},
	{
		name: 'Design Insights',
		id: 'design3',
		img: '',
		description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
		link: 'design3',
	}
];

export default class DesignNav extends Component {
	render(){
		const {currentURL} = this.props;
		return(
			<div style={{
				background: 'red',
				}}>
					<ul 					
					className='subnav-container'
					style={{display: 'flex'}}>					
					{items.map((d) => (
						<li 
						key={d.id} 
						style={{
							flex: '1',
							display: 'flex',
							alignItems: 'center',
							flexDirection: 'column'
						}}
						className={('/design/'+d.link === currentURL ? 'active' : '')}>
							<Link to={`/design/${d.link}`}
							style={{
								textTransform: 'capitalize',
								textAlign: 'center'
							}}
							>{d.name}</Link>
							{currentURL !== '/design' ?
								<p>CurrentURL = {currentURL}</p> : 
								<p>{d.description}</p>
							}
						</li>
					))}
				</ul>
			</div>
		)
	}
}