import React, {Component} from 'react';
import { Route } from 'react-router-dom';
import './Design.css';
import DesignNav from './DesignNav';
import Design1 from './DesignViews/Design1';
import Design2 from './DesignViews/Design2';
import Design3 from './DesignViews/Design3';
import DesignDefault from './DesignViews/DesignDefault';

export default class Design extends Component {
	render(){
		let currentURL = this.props.location.pathname;
		return(
			<div className="page-container">
				<div className="primary-container">

					<DesignNav currentURL={this.props.location.pathname} />
					<p>this.props.match.path = {this.props.match.path}</p>
					<p>currentURL = {currentURL}</p>
					<Route exact path={this.props.match.path} component={DesignDefault} />
      		<Route path={`${this.props.match.path}/design1`} component={Design1} />
      		<Route path={`${this.props.match.path}/design2`} component={Design2} />
      		<Route path={`${this.props.match.path}/design3`} component={Design3} />

				</div>
			</div>
		)
	}
}