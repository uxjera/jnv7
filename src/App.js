import React, { Component } from 'react';

import Navigation from './components/Navigation/Navigation';
import PageContainer from './containers/PageContainer';
import Footer from './components/Footer/Footer';

import './App.css';

class App extends Component {
	constructor(props){
		super(props)
		this.state={	}
	}

  render() {
    return (
      <div className="App">
        <Navigation />
        <PageContainer />
        <Footer />

      </div>
    );
  }
}

export default App;

// http://www.w3schools.com/css/trolltunga.jpg
// '../assets/images/background.png'